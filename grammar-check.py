# -*- coding: utf-8 -*-
"""
Created on Wed Apr 20 17:38:20 2022

@author: Chandrashekar
"""

'''
Steps..
1. Read input data from csv using pandas
2. Extract the relevant columns only
3. pass each comment to language tool and check for grammar correctness. 
4. If correct do nothing - mention No mistakes
5. If incorrect - as shown by the match count, mention mistakes found
6. Output is again a csv - with column 1 being text and column 2 being the number of mistakes in the text
'''

import pandas as pd


#df = pd.read_csv('C:\\Users\\Chakrapani\\Documents\\Next_ASO\\review_data.csv')
df = pd.read_csv('review_data.csv')
df.head()

cols = ['star', 'app_id','reviewDate']
df.drop(cols, axis=1, inplace=True)
df.head()


import language_tool_python
tool = language_tool_python.LanguageTool('en-US')
 
#text = """LanguageTool offers spell and grammar checking. Just paste your text here and click the 'Check Text' button. Click the colored phrases for details on potential errors. or use this text too see an few of of the problems that LanguageTool can detecd. What do you thinks of grammar checkers? Please not that they are not perfect. Style issues get a blue marker: It's 5 P.M. in the afternoon. The weather was nice on Thursday, 27 June 2017"""
for i in df.index:
    text = df['text'][i]
    matches = tool.check(text)
    count = len(matches)
    if count == 0:
        print(text," -- No mistakes")
    else:
        print(text," -- Mistakes found, ",count," mistakes")

tool.close()
